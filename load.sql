-- Ensure you are connected to the appropriate database

/*
Drop Existing Tables in Reverse Reference Order
DataRatesInfo -> FlowInfo
PacketInfo -> FlowInfo
PacketInfoExtended -> FlowInfo
SubFlowInfo -> FlowInfo
IATInfo -> FlowInfo
FlagInfo -> FlowInfo
AdditionalInfo -> FlowInfo
Annotations -> FlowInfo
FlowInfo -> L7Protocol
FlowInfo -> Endpoint
NamedIPs -> Endpoint
*/
drop table if exists DataRatesInfo;
drop table if exists PacketInfo;
drop table if exists PacketInfoExtended;
drop table if exists SubflowInfo;
drop table if exists IATInfo;
drop table if exists FlagInfo;
drop table if exists AdditionalInfo;
drop table if exists Annotations;
drop table if exists FlowInfo;
drop table if exists L7Protocol;
drop table if exists NamedIPs;
drop table if exists Endpoint;

-- Create Tables in Reference Order

create table Endpoint (
    IP varchar(15),
    Port int,
    Name varchar(30), -- Nullable
    primary key(IP, Port)
);

create table NamedIPs (
    IP varchar(15),
    Name varchar(30) not null,

    primary key (IP),
    foreign key (IP) references Endpoint(IP)
);

create index NamedIPs_Name_Index on NamedIPs (Name);

create table L7Protocol (
    L7Protocol int,
    Name varchar(30) unique not null,

    primary key (L7Protocol)
);

create index L7Protocol_Name_Index on L7Protocol (Name);

create table FlowInfo (
    Id int not null auto_increment,
    SourceIP varchar(15) not null,
    SourcePort int not null,
    DestinationIP varchar(15) not null,
    DestinationPort int not null,
    Timestamp datetime not null,
    Duration int not null,
    L7Protocol int not null,
    Protocol int not null,

    primary key (Id),
    foreign key (SourceIP, SourcePort) references Endpoint(IP, Port),
    foreign key (DestinationIP, DestinationPort) references Endpoint(IP, Port),
    foreign key (L7Protocol) references L7Protocol(L7Protocol)
);

create table Annotations (
    Id int,
    Annotation varchar(100) not null,

    primary key (Id),
    foreign key (Id) references FlowInfo(Id) on delete cascade
);

create table DataRatesInfo (
    Id int,
    ByteRate decimal(19, 8) not null,
    PacketRate decimal(19, 8) not null,
    FwdPacketRate decimal(19, 8) not null,
    BwdPacketRate decimal(19, 8) not null,

    primary key (Id),
    foreign key (Id) references FlowInfo(Id) on delete cascade
);

create table PacketInfo (
    Id int,
    TotalFwdPackets int not null,
    TotalBwdPackets int not null,
    TotalLengthFwdPackets int not null,
    TotalLengthBwdPackets int not null,

    primary key (Id),
    foreign key (Id) references FlowInfo(Id) on delete cascade
);

create table PacketInfoExtended (
    Id int,
    FwdPacketLengthMax int not null,
    FwdPacketLengthMin int not null,
    FwdPacketLengthMean decimal(9,5) not null,
    FwdPacketLengthStd  decimal(9,5) not null,

    BwdPacketLengthMax int not null,
    BwdPacketLengthMin int not null,
    BwdPacketLengthMean decimal(9,5) not null,
    BwdPacketLengthStd  decimal(9,5) not null,

    PacketLengthMax int not null,
    PacketLengthMin int not null,
    PacketLengthMean decimal(9,5) not null,
    PacketLengthStd  decimal(9,5) not null,
    PacketLengthVar  decimal(19,10) not null,

    primary key (Id),
    foreign key (Id) references FlowInfo(Id) on delete cascade
);

create table SubflowInfo (
    Id int,
    SubflowFwdPackets int not null,
    SubflowFwdBytes int not null,
    SubflowBwdPackets int not null,
    SubflowBwdBytes int not null,

    primary key (Id),
    foreign key (Id) references FlowInfo(Id) on delete cascade
);

create table IATInfo (
    Id int,
    FlowIATMax int not null,
    FlowIATMin int not null,
    FlowIATMean decimal(19, 10) not null,
    FlowIATStd  decimal(19,10) not null,
    
    FwdIATMax int not null,
    FwdIATMin int not null,
    FwdIATTotal int not null,
    FwdIATMean decimal(19,10) not null,
    FwdIATStd  decimal(19,10) not null,

    BwdIATMax int not null,
    BwdIATMin int not null,
    BwdIATTotal int not null,
    BwdIATMean decimal(19,10) not null,
    BwdIATStd  decimal(19,10) not null,

    primary key (Id),
    foreign key (Id) references FlowInfo(Id) on delete cascade
);

create table FlagInfo (
    Id int not null,
    FwdPSHFlag bool not null,
    BwdPSHFlag bool not null,

    FwdURGFlag bool not null,
    BwdURGFlag bool not null,

    FINFlag bool not null,
    SYNFlag bool not null,
    RSTFlag bool not null,
    PSHFlag bool not null,
    ACKFlag bool not null,
    URGFlag bool not null,
    CWEFlag bool not null,
    ECEFlag bool not null,


    primary key (Id),
    foreign key (Id) references FlowInfo(Id) on delete cascade
);

create table AdditionalInfo (
    Id int not null,
    FwdInitWinBytes int not null,
    BwdInitWinBytes int not null,
    FwdActDataPkt int not null,
    FwdMinSegSize int not null,

    ActiveMax int not null,
    ActiveMin int not null,
    ActiveMean decimal(19,8) not null,
    ActiveStd  decimal(19,8) not null,
    
    IdleMax int not null,
    IdleMin int not null,
    IdleMean decimal(19,8) not null,
    IdleStd  decimal(19,8) not null,

    Label char(6) not null,

    DownUpRatio int not null,

    primary key (Id),
    foreign key (Id) references FlowInfo(Id) on delete cascade
);


-- Load Data from CSV into Temp Table

drop table if exists temp_table;
create table temp_table (
    flow_id varchar(46), -- 15 + 5 + 15 + 5 + 2 + 4
    SourceIP varchar(15),
    SourcePort int,
    DestinationIP varchar(15), 
    DestinationPort int, 
    Protocol int,
    WeirdTimestamp char(18),
    Duration int, 

    TotalFwdPackets int,
    TotalBwdPackets int,
    TotalLengthFwdPackets int,
    TotalLengthBwdPackets int,

    FwdPacketLengthMax int,
    FwdPacketLengthMin int,
    FwdPacketLengthMean decimal(9,5),
    FwdPacketLengthStd  decimal(9,5),

    BwdPacketLengthMax int,
    BwdPacketLengthMin int,
    BwdPacketLengthMean decimal(9,5),
    BwdPacketLengthStd  decimal(9,5),

    ByteRate    decimal(19,8),
    PacketRate  decimal(19,8),

    FlowIATMean decimal(19,10),
    FlowIATStd  decimal(19,10),
    FlowIATMax  int,
    FlowIATMin  int,

    FwdIATTotal int,
    FwdIATMean  decimal(19,10),
    FwdIATStd   decimal(19,10),
    FwdIATMax   int,
    FwdIATMin   int,

    BwdIATTotal int,
    BwdIATMean  decimal(19,10),
    BwdIATStd   decimal(19,10),
    BwdIATMax   int,
    BwdIATMin   int,

    FwdPSHFlag bool,
    BwdPSHFlag bool,
    FwdURGFlag bool,
    BwdURGFlag bool,

    -- start unused
    FwdHeaderLength int,
    BwdHeaderLength int,
    -- end unused

	FwdPacketRate decimal(19,8),
    BwdPacketRate decimal(19,8),

	PacketLengthMax int,
    PacketLengthMin int,
    PacketLengthMean decimal(9,5),
    PacketLengthStd  decimal(9,5),
    PacketLengthVar  decimal(19,10),

    FINFlag bool,
    SYNFlag bool,
    RSTFlag bool,
    PSHFlag bool,
    ACKFlag bool,
    URGFlag bool,
    CWEFlag bool,
    ECEFlag bool,

    DownUpRatio int,

    -- start unused
    AveragePacketSize decimal(9,5),
    AvgFwdSegmentSize decimal(9,5),
    AvgBwdSegmentSize decimal(9,5),
    FwdHeaderLen int,
    FwdAvgBytesBulk decimal(9,5),
    FwdAvgPacketsBulk decimal(9,5),
    FwdAvgBulkRate decimal(9,5),
    BwdAvgBytesBulk decimal(9,5),
    BwdAvgPacketsBulk decimal(9,5),
    BwdAvgBulkRate decimal(9,5),
    -- end unused

    SubflowFwdPackets int,
    SubflowFwdBytes int,
    SubflowBwdPackets int,
    SubflowBwdBytes int,

    FwdInitWinBytes int,
    BwdInitWinBytes int,

    FwdActDataPkt int,
    FwdMinSegSize int,

    ActiveMean decimal(19,8),
    ActiveStd  decimal(19,8),
    ActiveMax  int,
    ActiveMin  int,

    IdleMean decimal(19,8),
    IdleStd  decimal(19,8),
    IdleMax  int,
    IdleMin  int,

    Label char(6),
    L7Protocol int,
    ProtocolName varchar(30)
);

load data infile '/var/lib/mysql-files/21-Network-Traffic/Dataset-Unicauca-Version2-87Atts.csv' ignore into table temp_table
		fields terminated by ','
		enclosed by '"'
		lines terminated by '\n'
        ignore 3077297 lines; -- Only take 500k rows

-- Add flow_id autoincrement
alter table temp_table 
    add Id int auto_increment primary key;

-- convert dates to datetimes
-- DD/MM/YYYYHH:MM:SS -> YYYY-MM-DD HH:MM:SS

alter table temp_table 
    add Timestamp datetime;

update temp_table t
    set t.Timestamp = CONVERT(
        CONCAT(
            SUBSTRING(t.WeirdTimestamp, 7, 4), -- Year
            '-',
            SUBSTRING(t.WeirdTimestamp, 4, 2), -- Month
            '-',
            SUBSTRING(t.WeirdTimestamp, 1, 2), -- Day
            ' ',
            SUBSTRING(t.WeirdTimestamp, 11, 8) -- Time
        ),    
        datetime
    )
;

-- BwdInitWinBytes -1 -> 0
update temp_table BwdInitWinBytes
    set BwdInitWinBytes = 0
    where BwdInitWinBytes = -1
;

drop table if exists temp_endpoints;
create table temp_endpoints (
    IP varchar(15),
    Port int
);

insert into temp_endpoints
    select distinct SourceIP, SourcePort from temp_table
;
insert into temp_endpoints
    select distinct DestinationIP, DestinationPort from temp_table
;

insert into Endpoint(IP, Port)
    select distinct IP, Port from temp_endpoints;

drop table temp_endpoints;

insert into L7Protocol
    select distinct L7Protocol, ProtocolName
    from temp_table
;

insert into FlowInfo
    select Id, SourceIP, SourcePort, DestinationIP, DestinationPort, Timestamp, Duration, L7Protocol, Protocol
    from temp_table
;

insert into DataRatesInfo
    select Id, ByteRate, PacketRate, FwdPacketRate, BwdPacketRate
    from temp_table
;

insert into PacketInfo
    select Id, TotalFwdPackets, TotalBwdPackets, TotalLengthFwdPackets, TotalLengthBwdPackets
    from temp_table
;

insert into PacketInfoExtended
    select 
        Id, 
        FwdPacketLengthMax,
        FwdPacketLengthMin,
        FwdPacketLengthMean,
        FwdPacketLengthStd,
        BwdPacketLengthMax,
        BwdPacketLengthMin,
        BwdPacketLengthMean,
        BwdPacketLengthStd,
        PacketLengthMax,
        PacketLengthMin,
        PacketLengthMean,
        PacketLengthStd,
        PacketLengthVar
    from temp_table
;

insert into SubflowInfo
    select Id, SubflowFwdPackets, SubflowFwdBytes, SubflowBwdPackets, SubflowBwdBytes
    from temp_table
;

insert into IATInfo
    select 
        Id,
        FlowIATMax,
        FlowIATMin,
        FlowIATMean,
        FlowIATStd,
        FwdIATMax,
        FwdIATMin,
        FwdIATTotal,
        FwdIATMean,
        FwdIATStd,
        BwdIATMax,
        BwdIATMin,
        BwdIATTotal,
        BwdIATMean,
        BwdIATStd
    from temp_table
;

insert into FlagInfo
    select 
        Id,
        FwdPSHFlag,
        BwdPSHFlag,
        FwdURGFlag,
        BwdURGFlag,
        FINFlag,
        SYNFlag,
        RSTFlag,
        PSHFlag,
        ACKFlag,
        URGFlag,
        CWEFlag,
        ECEFlag
    from temp_table
;

insert into AdditionalInfo
    select
        Id,
        FwdInitWinBytes,
        BwdInitWinBytes,
        FwdActDataPkt,
        FwdMinSegSize,
        ActiveMax,
        ActiveMin,
        ActiveMean,
        ActiveStd,
        IdleMax,
        IdleMin,
        IdleMean,
        IdleStd,
        Label,
        DownUpRatio
    from temp_table
;

drop table temp_table;
