#!/usr/bin/env python3
import configparser
import mysql.connector
import click
import csv
from getpass import getpass
import os
from tabulate import tabulate

# Global variables and constants
DATABASE = "networks"
CLI_KEYS = ["read", "update", "admin"]
FLOW_TABLES = ["AdditionalInfo", "DataRatesInfo", "FlagInfo", "FlowInfo", "IATInfo", "PacketInfo", "PacketInfoExtended", "SubflowInfo"]
FLOW_TABLES_NICKNAMES = ["Additional info", "Data rates", "Flags", "Flow info", "IAT info", "Packet info", "Extended packet info", "Subflow info"]
cli_key = None
conn = None
cursor = None

# Entry function. Handles DB closing
@click.group()
@click.pass_context
def cli(ctx):
    @ctx.call_on_close
    def close():
        global conn
        global cursor
        if cursor:
            cursor.close()
        if conn:
            conn.close()


# Group for list related functions
@cli.group(help="List values")
def list():
    pass

@list.command(help="List flow data with optional filtering")
@click.option('-i', '--id', help="Filter by flow ID.")
@click.option('-s', '--source', help="Filter by source IP.")
@click.option('--source-port', help="Filter by source port.", type=int)
@click.option('-d', '--destination', help="Filter by destination IP.")
@click.option('--destination-port', help="Filter by destination port.", type=int)
@click.option('--l7protocol', help="Filter by L7 protocol integer.", type=int)
@click.option('--start-date', help="Show data that occurs on or after a start date YYYY-MM-DD")
@click.option('--end-date', help="Show data that occurs on or before an end date YYYY-MM-DD")
@click.option('--endpoint-name', help="Filter by endpoint nickname")
@click.option('--ip-name', help="Filter by IP nickname")
@click.option('-l', '--limit', help="Limit the number of results", type=int)
@click.option('--sort-asc', help="Sort the data in ascending order based on an attribute")
@click.option('--sort-desc', help="Sort the data in descending order based on an attribute")
@click.option('-a', '--all-data', help="Show all data", default=False, is_flag=True)
@click.option('--rates-data', help="Show data on byte and packet rates", default=False, is_flag=True)
@click.option('--flags-data', help="Show flags data", default=False, is_flag=True)
@click.option('--iat-data', help="Show IAT data", default=False, is_flag=True)
@click.option('--packets-data', help="Show packet data", default=False, is_flag=True)
@click.option('--annotation', help="Show annotation", default=False, is_flag=True)
@click.option('-f', '--file', help="Name of file to write results to.")
def flow(id, source, source_port, destination, destination_port, l7protocol, start_date, end_date, endpoint_name, ip_name, limit, sort_asc, sort_desc, all_data, rates_data, flags_data, iat_data, packets_data, annotation, file):
    login()
    global cursor
    queryText = getQueryText(id, source, source_port, destination, destination_port, l7protocol, start_date, end_date, endpoint_name, ip_name, limit, sort_asc, sort_desc, all_data, rates_data, flags_data, iat_data, packets_data, annotation)
    cursor.execute(queryText)
    rows = cursor.fetchall()
    if all_data or rates_data or flags_data or iat_data or packets_data:
        print("Data is too wide to display on console")
    else:
        printRows(rows, cursor.column_names)
    write(file, rows)

@list.command(help="List endpoints with particular attributes")
@click.option('-i', '--ip', help="Specify IP.")
@click.option('-p', '--port', help="Specify port.", type=int)
@click.option('-n', '--name', help="Specify Endpoint name.")
@click.option('-f', '--file', help="Name of file to write results to.")
@click.option('-l', '--limit', help="Limit the number of results", type=int)
def endpoint(ip, port, name, file, limit):
    login()
    global cursor
    use_AND = False
    queryText = "select * from Endpoint"

    if ip:
        queryText += " WHERE"
        queryText += f" IP = '{ip}'"
        use_AND = True

    if port:
        if use_AND:
            queryText += " AND"
        else:
            queryText += " WHERE"
        queryText += f" Port = {port}"

    if name:
        if use_AND:
            queryText += " AND"
        else:
            queryText += " WHERE"
        queryText += f" Name = '{name}'"

    if limit:
        queryText += f" limit {limit}"

    cursor.execute(queryText)
    rows = cursor.fetchall()
    printRows(rows, cursor.column_names)
    write(file, rows)

@list.command(help="List IPs by name")
@click.option('-i', '--ip', help="Specify IP.")
@click.option('-n', '--name', help="Specify Endpoint name.")
@click.option('-f', '--file', help="Name of file to write results to.")
@click.option('-l', '--limit', help="Limit the number of results", type=int)
def ip(ip, name, file, limit):
    login()
    global cursor
    use_AND = False
    queryText = "select * from NamedIPs"

    if ip:
        queryText += " WHERE"
        queryText += f" IP = '{ip}'"
        use_AND = True

    if name:
        if use_AND:
            queryText += " AND"
        else:
            queryText += " WHERE"
        queryText += f" Name = '{name}'"

    if limit:
        queryText += f" limit {limit}"

    cursor.execute(queryText)
    rows = cursor.fetchall()
    printRows(rows, cursor.column_names)
    write(file, rows)


# Group for update related functions
@cli.group(help="Update a value")
def update():
    pass

@update.command(help="Update a value related to a flow record")
@click.option('-i', '--id', help="Filter by flow ID.")
@click.option('-s', '--source', help="Filter by source IP.")
@click.option('--source-port', help="Filter by source port.", type=int)
@click.option('-d', '--destination', help="Filter by destination IP.")
@click.option('--destination-port', help="Filter by destination port.", type=int)
@click.option('--l7protocol', help="Filter by L7 protocol integer.", type=int)
@click.option('--start-date', help="Show data that occurs on or after a start date YYYY-MM-DD")
@click.option('--end-date', help="Show data that occurs on or before an end date YYYY-MM-DD")
@click.option('--endpoint-name', help="Filter by endpoint nickname")
@click.option('--ip-name', help="Filter by IP nickname")
def flow(id, source, source_port, destination, destination_port, l7protocol, start_date, end_date, endpoint_name, ip_name):
    login()
    global conn
    global cursor
    global cli_key

    if cli_key not in ["update", "admin"]:
        print("You do not have permission to update records.")
        return

    flowID = getFlowID(id, source, source_port, destination, destination_port, l7protocol, start_date, end_date, endpoint_name, ip_name)
    if flowID == -1:
        return

    if input("Would you like to update this record? (y/n): ") != 'y':
        click.echo("Exiting.")
        return

    done = False
    while not done:
        click.echo(f"Given the following groups of data on this record: ")
        for i, v, in enumerate(FLOW_TABLES_NICKNAMES):
            click.echo(f"{i + 1}. {v}")
        choice = input("Which would you like to update? ")
        while choice not in [str(i+1) for i in range(len(FLOW_TABLES_NICKNAMES))]:
            choice = input("Invalid input, try again: ")

        click.echo("Here is the data: ")
        cursor.execute(f"select * from {FLOW_TABLES[int(choice) - 1]} where Id = {flowID}")
        rows = cursor.fetchall()
        printRows(rows, cursor.column_names)
        doneTable = False
        while not doneTable:
            attr = input("Which attribute would you like to update? ")
            while attr not in cursor.column_names or attr == "Id":
                attr = input("Not a valid attribute name, try again: ")
            val = input("New value: ")
            try:
                cursor.execute(f"update {FLOW_TABLES[int(choice)-1]} set {attr} = {val} where Id = {flowID}")
                conn.commit()
            except mysql.connector.Error as e:
                click.echo(f"ERROR: {e.msg}")
            cursor.execute(f"select * from {FLOW_TABLES[int(choice) - 1]} where Id = {flowID}")
            rows = cursor.fetchall()
            printRows(rows, cursor.column_names)
            if input("Would you like to update another attribute in this table? (y/n): ") != 'y':
                doneTable = True
        if input("Would you like to update another group of attributes for this record? (y/n): ") != 'y':
            done = True

@update.command(help="Update the name assigned to an endpoint")
@click.option('-i', '--ip', help="Specify IP.", prompt="Endpoint IP")
@click.option('-p', '--port', help="Specify port.", type=int, prompt="Endpoint Port")
@click.option('-n', '--name', help="Specify Endpoint name.", prompt="New endpoint name")
def endpoint(ip, port, name):
    login()
    global conn
    global cursor
    global cli_key

    if cli_key not in ["update", "admin"]:
        print("You do not have permission to update records.")
        return

    cursor.execute(f"select * from Endpoint where IP = '{ip}' and Port = {port}")
    rows = cursor.fetchall()
    if len(rows) == 0:
        click.echo("No endpoint matching that IP and port")
        return
    try:
        cursor.execute(f"update Endpoint set Name = '{name}' where IP = '{ip}' and Port = {port}")
        conn.commit()
        cursor.execute(f"select * from Endpoint where IP = '{ip}' and Port = {port}")
        rows = cursor.fetchall()
        printRows(rows, cursor.column_names)
        click.echo("Done.")
    except mysql.connector.Error as e:
        click.echo(f"ERROR: {e.msg}")

@update.command(help="Update the name assigned to an IP")
@click.option('-i', '--ip', help="IP.", prompt="IP")
@click.option('-n', '--name', help="New IP name", prompt="New IP name")
def ipname(ip, name):
    login()
    global conn
    global cursor
    global cli_key

    if cli_key not in ["update", "admin"]:
        print("You do not have permission to update records.")
        return

    cursor.execute(f"select * from NamedIPs where IP = '{ip}'")
    rows = cursor.fetchall()
    if len(rows) == 0:
        # need to add it to table
        try:
            cursor.execute(f"insert into NamedIPs values ('{ip}','{name}')")
            conn.commit()
            cursor.execute(f"select * from NamedIPs where IP = '{ip}'")
            rows = cursor.fetchall()
            printRows(rows, cursor.column_names)
            click.echo("Done.")
        except mysql.connector.IntegrityError:
            click.echo("ERROR: This IP does not exist in the database")
            click.echo("Operation cancelled")
        except mysql.connector.Error as e:
            click.echo(f"ERROR: {e.msg}")
        return
    printRows(rows, cursor.column_names)
    if input(f"Would you like to update this name to {name}? (y/n): ") != 'y':
        click.echo("Operation cancelled.")
        return
    try:
        cursor.execute(f"update NamedIPs set Name = '{name}' where IP = '{ip}'")
        conn.commit()
        cursor.execute(f"select * from NamedIPs where IP = '{ip}'")
        rows = cursor.fetchall()
        printRows(rows, cursor.column_names)
        click.echo("Done.")
    except mysql.connector.Error as e:
        click.echo(f"ERROR: {e.msg}")


# Group for delete related functions
@cli.group(help="Delete a value")
def delete():
    pass

@delete.command(help="Delete a value related to a flow record")
@click.option('-i', '--id', help="Filter by flow ID.")
@click.option('-s', '--source', help="Filter by source IP.")
@click.option('--source-port', help="Filter by source port.", type=int)
@click.option('-d', '--destination', help="Filter by destination IP.")
@click.option('--destination-port', help="Filter by destination port.", type=int)
@click.option('--l7protocol', help="Filter by L7 protocol integer.", type=int)
@click.option('--start-date', help="Show data that occurs on or after a start date YYYY-MM-DD")
@click.option('--end-date', help="Show data that occurs on or before an end date YYYY-MM-DD")
@click.option('--endpoint-name', help="Filter by endpoint nickname")
@click.option('--ip-name', help="Filter by IP nickname")
def flow(id, source, source_port, destination, destination_port, l7protocol, start_date, end_date, endpoint_name, ip_name):
    login()
    global conn
    global cursor
    global cli_key

    if cli_key not in ["admin"]:
        print("You do not have permission to delete records.")
        return

    flowID = getFlowID(id, source, source_port, destination, destination_port, l7protocol, start_date, end_date, endpoint_name, ip_name)
    if flowID == -1:
        return

    if input("Would you like to delete this record? (y/n): ") != 'y':
        click.echo("Exiting.")
        return

    try:
        cursor.execute(f"delete from FlowInfo where Id = {flowID}")
        conn.commit()
        click.echo(f"Deleted flow with id: {flowID}")
    except mysql.connector.Error as e:
        click.echo(f"ERROR: {e.msg}")

@delete.command(help="Delete the name assigned to an IP")
@click.option('-i', '--ip', help="IP.", prompt="IP")
def ipName(ip):
    login()
    global conn
    global cursor
    global cli_key

    if cli_key not in ["admin"]:
        click.echo("You do not have permission to delete records.")
        return

    cursor.execute(f"select * from NamedIPs where IP = '{ip}'")
    rows = cursor.fetchall()
    if len(rows) == 0:
        click.echo("This IP does not have a name. Nothing to delete.")
        return

    printRows(rows, cursor.column_names)
    if input("Would you like to delete this record? (y/n): ") != 'y':
        click.echo("Exiting.")
        return

    try:
        cursor.execute(f"delete from NamedIPs WHERE IP = '{ip}'")
        conn.commit()
        click.echo("Done.")
    except mysql.connector.Error as e:
        click.echo(f"ERROR: {e.msg}")

@delete.command(help="Delete an annotation for a flow")
@click.option('-i', '--id', help="Filter by flow ID.")
@click.option('-s', '--source', help="Filter by source IP.")
@click.option('--source-port', help="Filter by source port.", type=int)
@click.option('-d', '--destination', help="Filter by destination IP.")
@click.option('--destination-port', help="Filter by destination port.", type=int)
@click.option('--l7protocol', help="Filter by L7 protocol integer.", type=int)
@click.option('--start-date', help="Show data that occurs on or after a start date YYYY-MM-DD")
@click.option('--end-date', help="Show data that occurs on or before an end date YYYY-MM-DD")
@click.option('--endpoint-name', help="Filter by endpoint nickname")
@click.option('--ip-name', help="Filter by IP nickname")
def annotation(id, source, source_port, destination, destination_port, l7protocol, start_date, end_date, endpoint_name, ip_name):
    login()
    global conn
    global cursor
    global cli_key

    if cli_key not in ["admin"]:
        print("You do not have permission to delete records.")
        return

    flowID = getFlowID(id, source, source_port, destination, destination_port, l7protocol, start_date, end_date, endpoint_name, ip_name)
    if flowID == -1:
        return

    cursor.execute(f"select * from Annotations where Id = {flowID}")
    rows = cursor.fetchall()
    if len(rows) == 0:
        click.echo("This flow does not have an annotation to delete.")
        return

    printRows(rows, cursor.column_names)
    if input("Would you like to delete this record? (y/n): ") != 'y':
        click.echo("Exiting.")
        return

    try:
        cursor.execute(f"delete from Annotations WHERE Id = '{flowID}'")
        conn.commit()
        click.echo("Done.")
    except mysql.connector.Error as e:
        click.echo(f"ERROR: {e.msg}")


@cli.command(help="Add an annotation to a flow")
@click.option('-i', '--id', help="Filter by flow ID.")
@click.option('-s', '--source', help="Filter by source IP.")
@click.option('--source-port', help="Filter by source port.", type=int)
@click.option('-d', '--destination', help="Filter by destination IP.")
@click.option('--destination-port', help="Filter by destination port.", type=int)
@click.option('--l7protocol', help="Filter by L7 protocol integer.", type=int)
@click.option('--start-date', help="Show data that occurs on or after a start date YYYY-MM-DD")
@click.option('--end-date', help="Show data that occurs on or before an end date YYYY-MM-DD")
@click.option('--endpoint-name', help="Filter by endpoint nickname")
@click.option('--ip-name', help="Filter by IP nickname")
def annotate(id, source, source_port, destination, destination_port, l7protocol, start_date, end_date, endpoint_name, ip_name):
    login()
    global conn
    global cursor
    global cli_key

    if cli_key not in ["update", "admin"]:
        print("You do not have permission to update records.")
        return

    flowID = getFlowID(id, source, source_port, destination, destination_port, l7protocol, start_date, end_date, endpoint_name, ip_name)
    if flowID == -1:
        return

    cursor.execute(f"select * from Annotations where Id = {flowID}")
    rows = cursor.fetchall()
    if len(rows) == 1:
        click.echo("Annotation for this flow already exists:")
        printRows(rows, cursor.column_names)
        if input("Would you like to update the annotation? (y/n): ") != 'y':
            click.echo("Exiting.")
            return
        annotation = input("Annotation: ")
        while len(annotation) >= 100:
            annotation = input("Annotation more than 100 characters. Try again: ")
        try:
            cursor.execute(f"update Annotations set Annotation = '{annotation}' where Id = '{flowID}'")
            conn.commit()
            cursor.execute(f"select * from Annotations where Id = '{flowID}'")
            rows = cursor.fetchall()
            printRows(rows, cursor.column_names)
            click.echo("Done.")
        except mysql.connector.Error as e:
            click.echo(f"ERROR: {e.msg}")
        return

    annotation = input("Annotation: ")
    while len(annotation) >= 100:
        annotation = input("Annotation more than 100 characters. Try again: ")
    try:
        cursor.execute(f"insert into Annotations values ('{flowID}','{annotation}')")
        conn.commit()
        cursor.execute(f"select * from Annotations where Id = '{flowID}'")
        rows = cursor.fetchall()
        printRows(rows, cursor.column_names)
        click.echo("Done.")
    except mysql.connector.Error as e:
        click.echo(f"ERROR: {e.msg}")

# Util functions
def login():
    global cli_key
    setupDB()
    cli_key = getpass("CLI key: ")
    while cli_key not in CLI_KEYS:
        cli_key = getpass("Invalid CLI key. Re-enter: ")

def getQueryText(id, source, source_port, destination, destination_port, l7protocol, start_date, end_date, endpoint_name, ip_name, limit=None, sort_asc=None, sort_desc=None, all_data=None, rates_data=None, flags_data=None, iat_data=None, packets_data=None, annotation=None):
    use_AND = False
    use_COMMA = False
    queryText = "select * from FlowInfo"

    if all_data:
        queryText += " inner join DataRatesInfo using (Id) inner join FlagInfo using (Id) inner join IATInfo using (Id) inner join PacketInfo using (Id) left outer join Annotations using (Id)"
    else:
        if rates_data:
            queryText += " inner join DataRatesInfo using (Id)"
        if flags_data:
            queryText += " inner join FlagInfo using (Id)"
        if iat_data:
            queryText += " inner join IATInfo using (Id)"
        if packets_data:
            queryText += " inner join PacketInfo using (Id)"
        if annotation:
            queryText += " left outer join Annotations using (Id)"

    if id:
        queryText += f" WHERE Id = '{id}'"
        use_AND = True

    if start_date:
        if use_AND:
            queryText += " AND"
        else:
            queryText += " WHERE"
        queryText += f" Timestamp >= '{start_date}'"
        use_AND = True

    if end_date:
        if use_AND:
            queryText += " AND"
        else:
            queryText += " WHERE"
        queryText += f" Timestamp <= '{end_date} 23:59:59'"
        use_AND = True

    if source:
        if use_AND:
            queryText += " AND"
        else:
            queryText += " WHERE"
        queryText += f" SourceIP = '{source}'"
        use_AND = True

    if source_port:
        if use_AND:
            queryText += " AND"
        else:
            queryText += " WHERE"
        queryText += f" SourcePort = {source_port}"
        use_AND = True

    if destination:
        if use_AND:
            queryText += " AND"
        else:
            queryText += " WHERE"
        queryText += f" DestinationIP = '{destination}'"
        use_AND = True

    if destination_port:
        if use_AND:
            queryText += " AND"
        else:
            queryText += " WHERE"
        queryText += f" DestinationPort = {destination_port}"
        use_AND = True

    if l7protocol:
        if use_AND:
            queryText += " AND"
        else:
            queryText += " WHERE"
        queryText += f" L7Protocol = {l7protocol}"
        use_AND = True

    if endpoint_name:
        if use_AND:
            queryText += " AND"
        else:
            queryText += " WHERE"
        queryText = f"with endpoints as (select IP, Port from Endpoint where Name = '{endpoint_name}') " + queryText
        queryText += f" ((DestinationIP, DestinationPort) in (select * from endpoints) or (SourceIP, SourcePort) in (select * from endpoints))"
        use_AND = True
        use_COMMA = True

    if ip_name:
        if use_COMMA:
            queryText = f"with ips as (select IP from NamedIPs where Name = '{ip_name}'), " + queryText[4:]
        else:
            queryText = f"with ips as (select IP from NamedIPs where Name = '{ip_name}') " + queryText
        if use_AND:
            queryText += " AND"
        else:
            queryText += " WHERE"
        queryText += f" (DestinationIP in (select * from ips) or SourceIP in (select * from ips))"

    if sort_asc:
        queryText += f" order by {sort_asc} asc"
    elif sort_desc:
        queryText += f" order by {sort_desc} desc"

    if limit:
        queryText += f" limit {limit}"

    return queryText

def getFlowID(id, source, source_port, destination, destination_port, l7protocol, start_date, end_date, endpoint_name, ip_name):
    """
    :return: -1 if no Flow ID found
    """
    global cursor
    while True:
        queryText = getQueryText(id, source, source_port, destination, destination_port, l7protocol, start_date, end_date, endpoint_name, ip_name)
        cursor.execute(queryText)
        rows = cursor.fetchall()
        if len(rows) == 0:
            click.echo("No flows were found with these filters")
            return -1
        printRows(rows, cursor.column_names)
        if len(rows) == 1:
            return rows[0][0]

        click.echo(f"Found {len(rows)} rows that match these filters. Need to keep filtering.")
        click.echo("1. Flow id")
        click.echo("2. Source IP")
        click.echo("3. Source port")
        click.echo("4. Destination IP")
        click.echo("5. Destination port")
        click.echo("6. l7protocol integer")
        click.echo("7. Filter start date")
        click.echo("8. Filter end date")
        click.echo("9. Endpoint nickname")
        click.echo("10. IP nickname")
        choice = input("Which parameter would you like to update? ")
        while choice not in [str(i) for i in range(1, 11)]:
            choice = input("Invalid input, try again: ")
        if choice == '1':
            id = input("New flow id to search by: ")
        elif choice == '2':
            source = input("New source IP to search by: ")
        elif choice == '3':
            source_port = input("New source port to search by: ")
        elif choice == '4':
            destination = input("New destination IP to search by: ")
        elif choice == '5':
            destination_port = input("New destination port to search by: ")
        elif choice == '6':
            l7protocol = input("New l7protocol integer to search by: ")
        elif choice == '7':
            start_date = input("New start date to search by (YYYY-MM-DD): ")
        elif choice == '8':
            end_date = input("New end date to search by (YYYY-MM-DD): ")
        elif choice == '9':
            endpoint_name = input("New endpoint nickname to search by: ")
        elif choice == '10':
            ip_name = input("New IP nickname to search by: ")

def setupDB():
    path = '/'.join((os.path.abspath(__file__).replace('\\', '/')).split('/')[:-1])
    config = configparser.ConfigParser()
    if not config.read(os.path.join(path, 'config.ini')):
        config.add_section('database')
        config.set('database', 'host', input("Database host: "))
        config.set('database', 'port', input("Database port: "))
        config.set('database', 'db_name', input("Database name: "))
        config.set('database', 'username', input("Database username: "))
        config.set('database', 'password', getpass("Database password: "))

        if input("Save to config.ini file? (y/n): ") == "y":
            with open(os.path.join(path, 'config.ini'), 'w') as newini:
                config.write(newini)
            print("Saved database config to config.ini")

    try:
        global conn
        global cursor
        conn = mysql.connector.connect(host=config['database']['host'],
                                             port=config['database']['port'],
                                             database=config['database']['db_name'],
                                             user=config['database']['username'],
                                             password=config['database']['password'])
        cursor = conn.cursor()
    except mysql.connector.Error as e:
        print("Error while connecting to MySQL", e)
        exit()

def write(file, rows):
    if not file and input(f"Would you like to write these {len(rows)} rows to a file? (y/n): ") == "y":
        file = input("Output filename (include .csv): ")
    if file:
        with open(file, 'w', newline='') as f:
            fieldnames = []
            for i in cursor.column_names:
                fieldnames.append(i)
            writer = csv.writer(f)
            writer.writerow(fieldnames)
            writer.writerows(rows)
        click.echo(f"Wrote {len(rows)} rows to file: {file}")

def printRows(rows, cols):
    if len(rows) > 10:
        click.echo(f"Showing first 10/{len(rows)} rows:")
        print(tabulate(rows[:10], headers=cols, tablefmt='psql'))
    else:
        click.echo(f"Found {len(rows)} rows:")
        print(tabulate(rows, headers=cols, tablefmt='psql'))

if __name__ == '__main__':
    cli()
