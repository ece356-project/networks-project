# ECE356 Project

## Getting Started

### Server

1) Download the Unicauca dataset and place it in the following location: `/var/lib/mysql-files/21-Network-Traffic/Dataset-Unicauca-Version2-87Atts.csv`
2) Connect to your mysql server: `mysql -u {your username} -p -h {your database}`
3) If necessary, create a database to load the data into: `create database internet_traffic;`
4) Connect to your desired database: `use internet_traffic;`
5) Run the `load.sql` file: `source load.sql;` 

### Client

1) Install Python 3.9
2) Run `pip install -r requirements.txt`
3) Run `./cli.py` (or `python cli.py`)
4) Use CLI key: `read`, `update`, or `admin`

## Using the client

The client has `--help` menu options for all of its commands. Each command requires authorization using a CLI key, each which has its own scope. For this project, only 3 CLI keys have been created. For read only, use `read`. For reading and updating/inserting data use `update`. For all functionality, including deleting records, use `admin`.

#### help
The help menu is displayed when the `--help` flag is used or if no parameters are provided.
```
./cli.py
Usage: cli.py [OPTIONS] COMMAND [ARGS]...

Options:
  --help  Show this message and exit.

Commands:
  annotate  Add an annotation to a flow
  delete    Delete a value
  list      List values
  update    Update a value
```

#### list

This command is used to find information in the database satisfying required filters. If it has less than 10 columns it is outputed to the console. In all cases, an option to write the results to a csv file is provided. 

```
./cli.py list
Commands:
  endpoint  List endpoints with particular attributes
  flow      List flow data with optional filtering
  ip        List IPs by name
```

For exampling, listing all flows that occurred from 64.233.186.189 to 10.200.7.199 
```
./cli.py list flow -s 64.233.186.189 -d 10.200.7.199
CLI key: 
Showing first 10/44 rows:
+------+----------------+--------------+-----------------+-------------------+---------------------+------------+--------------+
|   Id | SourceIP       |   SourcePort | DestinationIP   |   DestinationPort | Timestamp           |   Duration |   L7Protocol |
|------+----------------+--------------+-----------------+-------------------+---------------------+------------+--------------|
|   69 | 64.233.186.189 |          443 | 10.200.7.199    |             34317 | 2017-05-15 05:36:27 |  112623704 |          126 |
|   90 | 64.233.186.189 |          443 | 10.200.7.199    |             34317 | 2017-05-15 05:38:49 |   52710804 |          126 |
|  252 | 64.233.186.189 |          443 | 10.200.7.199    |             34317 | 2017-05-15 05:40:09 |  111415468 |          126 |
|  328 | 64.233.186.189 |          443 | 10.200.7.199    |             34317 | 2017-05-15 05:42:29 |  109592828 |          126 |
|  353 | 64.233.186.189 |          443 | 10.200.7.199    |             34317 | 2017-05-15 05:44:46 |   55365796 |          126 |
|   13 | 64.233.186.189 |          443 | 10.200.7.199    |             36315 | 2017-05-15 05:35:51 |  108113645 |          126 |
|  127 | 64.233.186.189 |          443 | 10.200.7.199    |             36315 | 2017-05-15 05:38:09 |   81940016 |          126 |
|  247 | 64.233.186.189 |          443 | 10.200.7.199    |             36315 | 2017-05-15 05:39:55 |  116981358 |          126 |
|  300 | 64.233.186.189 |          443 | 10.200.7.199    |             36315 | 2017-05-15 05:42:24 |  104399908 |          126 |
|  380 | 64.233.186.189 |          443 | 10.200.7.199    |             36315 | 2017-05-15 05:44:30 |  107943442 |          126 |
+------+----------------+--------------+-----------------+-------------------+---------------------+------------+--------------+
Would you like to write these 44 rows to a file? (y/n): y
Output filename (include .csv): out.csv
Wrote 44 rows to file: out.csv
```

#### update

You can use update to change any flow data, an endpoint's nickname, or an IP's nickname.

```
./cli.py update 
Commands:
  endpoint  Update the name assigned to an endpoint
  flow      Update a value related to a flow record
  ipname    Update the name assigned to an IP
```

#### delete

You can use delete to delete an entire flow record, a flow's annotation, or an IP's nickname.

```
./cli.py delete 
Commands:
  annotation  Delete an annotation for a flow
  flow        Delete a value related to a flow record
  ipname      Delete the name assigned to an IP
```


